<?php

namespace Framework\Routing;
class Request
{
    private $path;
    private $get_params;
    private $post_params;
    private $type;

    public function __construct()
    {
        $this->path = $_GET['path'];
        $this->get_params = $_GET;
        unset($this->get_params['path']);
        $this->post_params = $_POST;
        if($_SERVER['REQUEST_METHOD'] === 'POST') $this->type = Route::METHOD_POST;
        if($_SERVER['REQUEST_METHOD'] === 'GET') $this->type = Route::METHOD_GET;
    }

    public function getPath() {
        return $this->path;
    }

    public function getGet_params() {
        return $this->get_params;
    }

    public function getPost_params() {
        return $this->post_params;
    }

    public function getType() {
        return $this->type;
    }
}